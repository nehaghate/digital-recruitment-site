package com.ng.neu;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ng.neu.dao.StudentDao;
import com.ng.neu.dao.UserDAO;
import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Student;
import com.ng.neu.pojo.User;

@Controller
@RequestMapping("/register1.htm")

public class NewController1 {
	@RequestMapping(method = RequestMethod.POST)
	protected String doSubmitAction(@ModelAttribute("user") Student s, BindingResult result) throws Exception {
		
		try {
			//System.out.print("test");
			//System.out.print("test1");
			
			
			StudentDao sdao = new StudentDao();
			System.out.print(s.getName());
			System.out.print(s.getEmail().getEmailId());
			
			sdao.createStudent(s.getName(),s.getEmail().getEmailId());
			// DAO.close();
		} catch (AdException e) {
			System.out.println("Exception: " + e.getMessage());
		}

		return "addedUser";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String initializeForm(@ModelAttribute("user") Student s, BindingResult result) {

		return "reg";
	}

}
