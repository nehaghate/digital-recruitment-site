package com.ng.neu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ng.neu.dao.JobDAO;
import com.ng.neu.dao.JobCategoryDAO;
import com.ng.neu.dao.UserDAO;
import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Job;
import com.ng.neu.pojo.JobCategory;
import com.ng.neu.pojo.User;

@Controller
@RequestMapping("/addjob.htm")
public class PostaJobFormController {

	@Autowired
	@Qualifier("jobValidator")
	JobValidator jobValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder)
	{
		binder.setValidator(jobValidator);
	}
	
	@RequestMapping(method=RequestMethod.POST)
    protected String doSubmitAction(@ModelAttribute("advert")Job job,BindingResult result) throws Exception{

		jobValidator.validate(job, result);
		if(result.hasErrors())
		{
			return "addJobForm";
		}
    	
    	
        String username = job.getPostedBy();   //get posting user from addAdvertForm
        String categoryTitle = job.getCategory_name();   //get category user from addAdvertForm
        String title = job.getTitle();      //get job title user from addAdvertForm
        String jobDescription = job.getJobDescription();    //get user message from addAdvertForm
        String jobType = job.getJobType();
        String jobExperience = job.getJobExperience();
        String jobLocation = job.getJobLocation();

        try {
            UserDAO users = new UserDAO();
            JobCategoryDAO categories = new JobCategoryDAO();
            JobDAO jobs = new JobDAO();

            //searching from database
            User user = users.getUser(username);

            //searching from database
            JobCategory category = categories.get(categoryTitle);

            //insert a new job
            Job adv = jobs.create(title, jobDescription, user, category.getId(),category.getTitle(),jobType,jobExperience,jobLocation);

            category.addJob(adv);
            
            categories.save(category);

            
        } catch (AdException e) {
            System.out.println(e.getMessage());
        }
        return "addedJob";
    }
    
	@RequestMapping(method=RequestMethod.GET)
    public String initializeForm(@ModelAttribute("advert")Job advert, BindingResult result) { 
   
        return "addJobForm"; 
    } 
}