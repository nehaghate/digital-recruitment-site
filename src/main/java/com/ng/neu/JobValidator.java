package com.ng.neu;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ng.neu.pojo.Job;

@Component
public class JobValidator implements Validator {

    public boolean supports(Class aClass)
    {
        return aClass.equals(Job.class);
    }

    public void validate(Object obj, Errors errors)
    {
        Job job = (Job) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.invalid.job", "Job Title Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "jobLocation", "error.invalid.job", "Job Location Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "jobDescription", "error.invalid.job", "Job Description Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postedBy", "error.invalid.postedBy", "Authorization Required");
        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.invalid.category", "Category Required");
    }
}
