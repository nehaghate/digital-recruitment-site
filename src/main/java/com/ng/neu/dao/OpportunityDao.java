package com.ng.neu.dao;

import org.hibernate.HibernateException;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Job;
import com.ng.neu.pojo.Opportunity;
import com.ng.neu.pojo.Student;
import com.ng.neu.pojo.User;

public class OpportunityDao  extends DAO {
	
	
	public Opportunity createOpportunity(String title, String message )
            throws AdException {
        try {
            begin();
            Opportunity opportunity = new Opportunity(title, message);
            getSession().save(opportunity);
            commit();
            return opportunity;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create advert", e);
            throw new AdException("Exception while creating job: " + e.getMessage());
        }
    }

}
