package com.ng.neu.dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.JobCategory;

public class JobCategoryDAO extends DAO {

    public JobCategory get(String title) throws AdException {
        try {
            begin();
            Query q=getSession().createQuery("from JobCategory where title= :title");
            q.setString("title",title);
            JobCategory category=(JobCategory)q.uniqueResult();
            commit();
            return category;
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not obtain the named category " + title + " " + e.getMessage());
        }
    }

    public List list() throws AdException {
        try {
            begin();
            Query q = getSession().createQuery("from JobCategory");
            List list = q.list();
            commit();
            return list;
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not list the categories", e);
        }
    }

    public JobCategory create(String title) throws AdException {
        try {
            begin();
            JobCategory cat = new JobCategory(title);
            getSession().save(cat);
            commit();
            return null;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create the category", e);
            throw new AdException("Exception while creating category: " + e.getMessage());
        }
    }

    public void save(JobCategory category) throws AdException {
        try {
            begin();
            getSession().update(category);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not save the category", e);
        }
    }

    public void delete(JobCategory category) throws AdException {
        try {
            begin();
            getSession().delete(category);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not delete the category", e);
        }
    }
}