package com.ng.neu.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Email;
import com.ng.neu.pojo.Employer;

public class EmployerDAO extends DAO{

	public EmployerDAO(){
		
	}
	
	 public Employer getEmployer(String username)
	            throws AdException {
	        try {
	            begin();
	            Query q = getSession().createQuery("from Employer where name = :username");
	            q.setString("username", username);
	            Employer employer = (Employer) q.uniqueResult();
	            commit();
	            return employer;
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not get employer " + username, e);
	        }
	    }
	 
	 
	 public ArrayList<Employer> getEmployerList() throws AdException{
		 
		 try {
	            begin();
	            Query q = getSession().createQuery("from Employer");
	            ArrayList<Employer> employerList = (ArrayList<Employer>) q;
	            commit();
	            return employerList;
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not get employerlist " , e);
	        }
		 
		 
	 }
	 
	 
	 public Employer createEmployer(String username, String password,String emailId, 
			 String firstName, String lastName,String companyName, String location)
	            throws AdException {
	        try {
	            begin();
	            //System.out.println("inside DAO");
	            
	            Email email=new Email(emailId);
	            Employer employer = new Employer( companyName,  location);
	            
	            
	            employer.setFirstName(firstName);
	            employer.setLastName(lastName);
	            employer.setEmail(email);
	            employer.setName(username);
	            employer.setPassword(password);
	            
	            email.setUser(employer);
	            
	            getSession().save(employer);
	            commit();
	            
	            return employer;
	            
	        } catch (HibernateException e) {
	            rollback();
	            //throw new AdException("Could not create user " + username, e);
	            throw new AdException("Exception while creating employer: " + e.getMessage());
	        }
	    }
	 
	 public void deleteEmployer(Employer employer)
	            throws AdException {
	        try {
	            begin();
	            employer.setStatus(false);
	            getSession().save(employer);
	           // getSession().delete(employer);
	            commit();
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not delete user " + employer.getName(), e);
	        }
	    }

}
