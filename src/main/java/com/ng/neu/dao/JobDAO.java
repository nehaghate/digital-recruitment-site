package com.ng.neu.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Job;
import com.ng.neu.pojo.User;

public class JobDAO extends DAO {

    public Job create(String title, String message, User user, long category_id,String categoryName,
    		String jobType,String jobExperience,String jobLocation)
            throws AdException {
        try {
            begin();
            Job job = new Job(title, message, user,category_id,categoryName,jobExperience,jobType,jobLocation);
            getSession().save(job);
            commit();
            return job;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create advert", e);
            throw new AdException("Exception while creating job: " + e.getMessage());
        }
    }
    
    public Job editaJob(String title, String message, User user, long category_id,String categoryName,
    		String jobType,String jobExperience,String jobLocation)
            throws AdException {
        try {
            begin();
           // Job job = new Job(title, message, user,category_id,categoryName,jobExperience,jobType,jobLocation);
            //getSession().save(job);
            Query q = getSession().createQuery("from Job where title = :title");
            q.setString("title", title);
            Job editJob = (Job) q.uniqueResult();
            editJob.setJobDescription(message);
            editJob.setCategory(category_id);
            editJob.setCategory_name(categoryName);
            editJob.setJobExperience(jobExperience);
            editJob.setJobType(jobType);
            editJob.setJobLocation(jobLocation);
            
            commit();
            return editJob;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create advert", e);
            throw new AdException("Exception while editing job: " + e.getMessage());
        }
    }
    
    
    public Job applygetJob(String jobid)
            throws AdException {
        try {
            begin();
           // Job job = new Job(title, message, user,category_id,categoryName,jobExperience,jobType,jobLocation);
            //getSession().save(job);
            Query q = getSession().createQuery("from Job where jobid = :jobid");
            q.setString("jobid", jobid);
            Job job = (Job) q.uniqueResult();
            commit();
            return job;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create advert", e);
            throw new AdException("Exception while applying job: " + e.getMessage());
        }
    }
    
    
    

    public void delete(Job job)
            throws AdException {
        try {
            begin();
            getSession().delete(job);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not delete job", e);
        }
    }
}