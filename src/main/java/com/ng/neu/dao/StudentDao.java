package com.ng.neu.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Email;
import com.ng.neu.pojo.Student;
import com.ng.neu.pojo.User;

public class StudentDao extends DAO {
	
	
	public StudentDao(){
		
	}

	public Student getStudent(String username)
            throws AdException {
        try {
            begin();
            Query q = getSession().createQuery("from Student where name = :username");
            q.setString("username", username);
            Student student = (Student) q.uniqueResult();
            commit();
            return student;
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not get user " + username, e);
        }
    }
	
	
	public Student createStudent(String firstName,String emailId)
            throws AdException {
        try {
            begin();
            System.out.println("inside DAO");
            
            Email email=new Email(emailId);
            Student student=new Student(firstName);
            
            student.setFirstName(firstName);
            student.setEmail(email);
            
            email.setStudent(student);
            
            getSession().save(student);
            
            commit();
            return student;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create user " + username, e);
            throw new AdException("Exception while creating user: " + e.getMessage());
        }
    }
}
