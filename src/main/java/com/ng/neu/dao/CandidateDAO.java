package com.ng.neu.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Candidate;
import com.ng.neu.pojo.Email;
import com.ng.neu.pojo.Employer;

public class CandidateDAO extends DAO {
	
	public CandidateDAO(){
		
	}
	
	 public Candidate getCandidate(String username)
	            throws AdException {
	        try {
	            begin();
	            Query q = getSession().createQuery("from Candidate where name = :username");
	            q.setString("username", username);
	            Candidate candidate = (Candidate) q.uniqueResult();
	            commit();
	            
	            return candidate;
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not get candidate " + username, e);
	        }
	    }
	 
	 
	 public Candidate createCandidate(String username, String password,String emailId, 
			 String firstName, String lastName,String universityName)
	            throws AdException {
	        try {
	            begin();
	            //System.out.println("inside DAO");
	            
	            Email email=new Email(emailId);
	            Candidate candidate = new Candidate(universityName);
	            
	            
	            candidate.setFirstName(firstName);
	            candidate.setLastName(lastName);
	            candidate.setEmail(email);
	            candidate.setName(username);
	            candidate.setPassword(password);
	            
	            email.setUser(candidate);
	            
	            getSession().save(candidate);
	            commit();
	            
	            return candidate;
	            
	        } catch (HibernateException e) {
	            rollback();
	            //throw new AdException("Could not create user " + username, e);
	            throw new AdException("Exception while creating candidate: " + e.getMessage());
	        }
	    }
	 
	 public void deleteCandidate(Candidate candidate)
	            throws AdException {
	        try {
	            begin();
	            candidate.setStatus(false);
	            getSession().save(candidate);
	           // getSession().delete(employer);
	            commit();
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not delete candidate " + candidate.getName(), e);
	        }
	    }

}
