package com.ng.neu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ng.neu.dao.JobCategoryDAO;
import com.ng.neu.dao.JobDAO;
import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Job;
import com.ng.neu.pojo.JobCategory;
import com.ng.neu.pojo.User;

@Controller

public class ListJobsController{
	
	@Autowired
	@Qualifier("jobValidator")
	JobValidator jobValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder)
	{
		binder.setValidator(jobValidator);
	}

	@RequestMapping(value="/listjobs.htm" ,method=RequestMethod.GET)
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobCategoryDAO categories = null;
        List categoryList = null;
        List jobsList = new ArrayList();
        HttpSession session =request.getSession();

        try {
            categories = new JobCategoryDAO();
            categoryList = categories.list();

            Iterator categIterator = categoryList.iterator();

            while (categIterator.hasNext())
            {
                JobCategory category = (JobCategory) categIterator.next();

                Iterator advIterator = category.getJobs().iterator();

                while (advIterator.hasNext())
                {
                    Job advert = (Job) advIterator.next();
                    jobsList.add(advert);
                }
            }
            
            session.setAttribute("noofjobs", jobsList.size());
            //DAO.close();
        } catch (AdException e) {
            System.out.println(e.getMessage());
        }

        ModelAndView mv = new ModelAndView("viewJobs", "jobs", jobsList);
        return mv;
    }
	
	
	
	@RequestMapping(value="/editjob.htm" ,method=RequestMethod.POST)
    protected ModelAndView editJob(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		String jobDescription = request.getParameter("jobdescription");
		String jobLocation = request.getParameter("joblocation");
		
		ModelAndView mv = new ModelAndView("addedJob");		
	
		return mv;
	}
	
	
	@RequestMapping(value="/viewjobscandidate.htm" ,method=RequestMethod.GET)
    protected ModelAndView candidateListJobs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobCategoryDAO categories = null;
        List categoryList = null;
        List jobsList = new ArrayList();
        HttpSession session =request.getSession();

        try {
            categories = new JobCategoryDAO();
            categoryList = categories.list();

            Iterator categIterator = categoryList.iterator();

            while (categIterator.hasNext())
            {
                JobCategory category = (JobCategory) categIterator.next();

                Iterator advIterator = category.getJobs().iterator();

                while (advIterator.hasNext())
                {
                    Job job = (Job) advIterator.next();
                  //  System.out.println(job.get);
                    jobsList.add(job);
                }
            }
            
            session.setAttribute("noofjobs", jobsList.size());
            //DAO.close();
        } catch (AdException e) {
            System.out.println(e.getMessage());
        }

        ModelAndView mv = new ModelAndView("candidateViewJobs", "jobs", jobsList);
        return mv;
        
    }
	
	@RequestMapping(value="/applyjob.htm" ,method=RequestMethod.GET)
    protected ModelAndView candidateApplyJob(HttpServletRequest request, HttpServletResponse response) throws Exception {
   
		String jobid = request.getParameter("jobid");
		String candidateid = request.getParameter("candidateid");
		 HttpSession session =request.getSession();
		 
		 User candidate = (User) session.getAttribute("user");
		 JobDAO jobDAO = new JobDAO();
		 Job job = jobDAO.applygetJob(jobid);
		 
		 candidate.addJob(job);
		 
		 System.out.println("sucess apply job");
		
		
		 ModelAndView mv = new ModelAndView("candidateViewJobs","success",true);
	        return mv;
	}
	
}