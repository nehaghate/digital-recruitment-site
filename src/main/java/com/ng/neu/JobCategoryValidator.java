package com.ng.neu;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ng.neu.pojo.JobCategory;

@Component
public class JobCategoryValidator implements Validator {

    public boolean supports(Class aClass)
    {
        return aClass.equals(JobCategory.class);
    }

    public void validate(Object obj, Errors errors)
    {
        JobCategory newCategory = (JobCategory) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.invalid.category", "Category Required");
    }
}
