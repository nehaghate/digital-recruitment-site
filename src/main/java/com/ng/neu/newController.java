package com.ng.neu;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ng.neu.dao.JobCategoryDAO;
import com.ng.neu.dao.JobDAO;
import com.ng.neu.dao.OpportunityDao;
import com.ng.neu.dao.StudentDao;
import com.ng.neu.dao.UserDAO;
import com.ng.neu.exception.AdException;
import com.ng.neu.pojo.Job;
import com.ng.neu.pojo.JobCategory;
import com.ng.neu.pojo.Opportunity;
import com.ng.neu.pojo.Student;
import com.ng.neu.pojo.User;

@Controller
@RequestMapping("/addjobnew.htm")

public class newController {
	
	@RequestMapping(method=RequestMethod.POST)
    protected String doSubmitAction(@ModelAttribute("oppo")Opportunity job,BindingResult result) throws Exception{

		
        String title = job.getTitle();      //get job title user from addAdvertForm
        String jobDescription = job.getJobDescription();    //get user message from addAdvertForm
        String username = job.getPostedBy();   //get posting user from addAdvertForm
        
        try {
            
            StudentDao stu = new StudentDao();

            OpportunityDao oppors = new OpportunityDao();

            //insert a new job
            Opportunity o = oppors.createOpportunity(title, jobDescription);
            
            Student s =stu.getStudent(username);
           
            s.addOpportunity(o);
            
            
        } catch (AdException e) {
            System.out.println(e.getMessage());
        }
        return "addedJob";
    }
    
	@RequestMapping(method=RequestMethod.GET)
    public String initializeForm(@ModelAttribute("oppo")Opportunity job, BindingResult result) { 
   
        return "file"; 
    }

}
