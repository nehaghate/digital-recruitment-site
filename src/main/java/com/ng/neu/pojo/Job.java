package com.ng.neu.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name= "jobtable")
public class Job {

    @Id
    @GeneratedValue
    @Column(name = "jobid", unique=true, nullable = true)
   private long id;
    
    @Column(name = "title")
   private String title;
    
    @Column(name = "message")
   private String jobDescription;
    
    @Column(name = "jobexperience")
    private String jobExperience;
    
    @Column(name = "jobType")
    private String jobType;
    
    @Column(name = "jobLocation")
    private String jobLocation;
    
    @Column(name="jobstatus")
    private String jobStatus;
    
    @Column(name="isdeleted")
    private boolean jobdeleted;
    
    @Column(name="jobpostedon")
    private Date jobpostedon;
    

	@Transient  //which are not saved database but will be present in the scope
   private String postedBy;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
   private User user;
   
    @Transient
   private String category_name;
   
    @JoinColumn(name = "categoryid")
   private long category;
    
    public Job(String title, String message, User user,long category_id,
 		   String categoryName,String jobExperience,
 		   String jobType, String jobLocation) {
        this.title = title;
        this.jobDescription = message;
        this.user = user;
        this.category = category_id;
        this.category_name=categoryName;
        this.jobExperience=jobExperience;
        this.jobType = jobType;
        this.jobLocation = jobLocation;
        this.jobdeleted= false;
        this.jobStatus="open";
        this.jobpostedon = new Date();
    }

    Job() {
    }
    
    
    
    public Date getJobpostedon() {
		return jobpostedon;
	}

	public void setJobpostedon(Date jobpostedon) {
		this.jobpostedon = jobpostedon;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public boolean isJobdeleted() {
		return jobdeleted;
	}

	public void setJobdeleted(boolean jobdeleted) {
		this.jobdeleted = jobdeleted;
	}

	public String getJobExperience() {
		return jobExperience;
	}

	public void setJobExperience(String jobExperience) {
		this.jobExperience = jobExperience;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}



   public String getTitle() {
       return title;
   }

   public void setTitle(String title) {
       this.title = title;
   }

   public User getUser() {
       return user;
   }

   public void finalize() throws Throwable {}; void setUser(User user) {
       this.user = user;
   }

   public long getId() {
       return id;
   }

   protected void setId(long id) {
       this.id = id;
   }

   public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getPostedBy() {
       return this.postedBy;
   }

   public void setPostedBy(String pb) {
       this.postedBy = pb;
   }

    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }
}