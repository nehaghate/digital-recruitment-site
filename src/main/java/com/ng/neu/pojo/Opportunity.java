package com.ng.neu.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "opportunitytable")

public class Opportunity {

	@Id
	@GeneratedValue
	@Column(name = "jobid", unique = true, nullable = true)
	private long jobid;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String jobDescription;

	@Transient  //which are not saved database but will be present in the scope
	   private String postedBy;
	   
	public Opportunity(){
		
	}
	
	public String getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public Opportunity(String title, String jobDescription) {
		this.title = title;
		this.jobDescription = jobDescription;
	}

	@ManyToMany(mappedBy = "jobsList")
	private Set<Student> studentsList = new HashSet<Student>();

	
	
	public long getJobid() {
		return jobid;
	}

	public void setJobid(long jobid) {
		this.jobid = jobid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public Set<Student> getStudentsList() {
		return studentsList;
	}

	public void setStudentsList(Set<Student> studentsList) {
		this.studentsList = studentsList;
	}

}
