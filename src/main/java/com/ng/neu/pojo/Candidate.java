package com.ng.neu.pojo;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "candidatetable")
@PrimaryKeyJoinColumn(name = "personID")
public class Candidate extends User {

	public Candidate() {

	}

	public Candidate(String universityName) {
		this.universityName = universityName;
	}

	@Id
	@GeneratedValue
	@Column(name = "candidateID", unique = true, nullable = false)
	private long candidateID;

	@Column(name = "university")
	private String universityName;

	private ArrayList<Job> jobList;

	public ArrayList<Job> getJobList() {
		return jobList;
	}

	public void setJobList(ArrayList<Job> jobList) {
		this.jobList = jobList;
	}

	public long getCandidateID() {
		return candidateID;
	}

	public void setCandidateID(long candidateID) {
		this.candidateID = candidateID;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

}
