package com.ng.neu.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="usertable")
@PrimaryKeyJoinColumn(name="personID")
public class User extends Person{
	
	@Column(name="name")
    private String name;
	
	@Column(name="password")
    private String password;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="role")
	private String role;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy="user")
	private Set<Job> jobs = new HashSet<Job>();

	@OneToOne(fetch=FetchType.EAGER,mappedBy="user",cascade=CascadeType.ALL)
    private Email email;
	

    public User(String name, String password,String role) {
        this.name = name;
        this.password = password;
        this.role = role;
        this.jobs = new HashSet<Job>();
    }

    public User() {
    }
    
    public void addJob(Job job) {
        getJobs().add(job);
    }
    
	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


	public Email getEmail() {
		return email;
		
	}

	public void setEmail(Email email) {
		this.email = email;
	}

}