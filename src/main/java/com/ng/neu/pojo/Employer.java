package com.ng.neu.pojo;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name="emptable")
@PrimaryKeyJoinColumn(name="personID")

public class Employer extends User {

	public Employer(){
		
	}
	
	public Employer(String companyName, String location){
		this.companyName = companyName;
		this.location = location;
		
	}
	
	@Id
	@GeneratedValue
	@Column(name="empId",unique=true,nullable=false)
	private long empId;
	
	@Column(name="companyname")
	private String companyName;
	
	@Column(name="location")
	private String location;
	
	private ArrayList<Job> jobList;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ArrayList<Job> getJobList() {
		return jobList;
	}

	public void setJobList(ArrayList<Job> jobList) {
		this.jobList = jobList;
	}
	
	
	
}
