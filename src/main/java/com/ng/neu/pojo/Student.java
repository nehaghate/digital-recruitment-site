package com.ng.neu.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="personID")
public class Student extends Person{

	

	public Student(){
		
	}
	@Column(name="name")
    private String name;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Opportunity> jobsList = new HashSet<Opportunity>();
	
	 public void addOpportunity(Opportunity opportunity) {
	        jobsList.add( opportunity );
	        opportunity.getStudentsList().add( this );
	    }
	
	@OneToOne(fetch=FetchType.EAGER,mappedBy="student",cascade=CascadeType.ALL)
    private Email email;
	
	
	public void setName(String name) {
		this.name = name;
	}

	public Set<Opportunity> getJobsList() {
		return jobsList;
	}

	public void setJobsList(Set<Opportunity> jobsList) {
		this.jobsList = jobsList;
	}

	
	public Student(String name) {
        this.name = name;
       
        this.jobsList = new HashSet<Opportunity>();
    }
	
	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

}
