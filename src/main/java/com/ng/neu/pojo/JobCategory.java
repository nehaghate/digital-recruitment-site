package com.ng.neu.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "categorytable")
public class JobCategory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="categoryId",unique=true,nullable=true)
    private long id;
	
	@Column(name="title")
    private String title;
    
	@OneToMany(fetch = FetchType.LAZY,mappedBy="category")
	private Set<Job> jobs = new HashSet<Job>();

    public JobCategory(String title) {
        this.title = title;
        this.jobs = new HashSet<Job>();
    }

    	JobCategory() {
    }
    	
    	  public void addJob(Job job) {
    	        getJobs().add(job);
    	    }

    public Set<Job> getJobs() {
			return jobs;
		}

		public void setJobs(Set<Job> jobs) {
			this.jobs = jobs;
		}


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}