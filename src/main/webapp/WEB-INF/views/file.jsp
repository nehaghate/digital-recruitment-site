<%@page import="com.ng.neu.pojo.JobCategory"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<html>
<head>
<title>Add Job Form</title>
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/javascript"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Recruit.ed</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="addcategory.htm">Add Job Category</a></li>
						<li class="active"><a href="addjob.htm">Post a Job</a></li>
						<li><a href="listjobs.htm">List Jobs<span class="badge">${sessionScope.noofjobs}</span></a></li>
						<li><a href="companyprofile.htm">Profile</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="./">Questions? Call us at
								1-866-204-6764 <span class="sr-only">(current)</span>
						</a></li>
						<li>Welcome, ${sessionScope.user.firstName}</li>

						<li><a href="logout.htm" class="btn btn-info" role="button">Logout</a></li>

					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<h2>Posting a New Job</h2>

		<form:form action="addjobnew.htm" commandName="oppo" method="post">

	<div class="row">
				<div class="input-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<form:input path="title" class="form-control"
						placeholder="Job title" size="100" />
					<font color="red"><form:errors path="title" /></font>
				</div>
			</div>


			<div class="row">
				<div class="input-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<form:textarea path="jobDescription" class="form-control"
						placeholder="Job Description" size="300" />
					<font color="red"><form:errors path="jobDescription" /></font>
				</div>
			</div>

	<div class="row">

				<div class="input-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<form:input path="postedBy" class="form-control"
						placeholder="Job Posted By" size="100" />
					<font color="red"><form:errors path="postedBy" /></font>
				</div>
			</div>


			<div class="row">
				<input type="submit" class="btn btn-default" value="Post a Job" />
			</div>


		</form:form>
	</div>



</body>
</html>
