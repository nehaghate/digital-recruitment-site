<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register User Form</title>
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/javascript"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">

</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Recruit.ed</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="login.htm">Register User</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="./">Questions? Call us at
								1-866-204-6764 <span class="sr-only">(current)</span>
						</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<div class="row">

			<div class="col-md-6">
				<form:form action="register1.htm" commandName="user" method="post">

				
					<div class="input-group">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						<form:input path="firstName" class="form-control"
							placeholder="First Name" size="100" />
						<font color="red"><form:errors path="firstName" /></font>
					</div>

					<div class="input-group">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						<form:input path="lastName" class="form-control"
							placeholder="Last Name" size="100" />
						<font color="red"><form:errors path="lastName" /></font>
					</div>

					<div class="input-group">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						<form:input path="name" class="form-control"
							placeholder="Username" size="100" />
						<font color="red"><form:errors path="name" /></font>
					</div>

				
					<div class="input-group">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						<form:input path="email.emailId" class="form-control"
							placeholder="Email ID" size="100" />
						<font color="red"><form:errors path="email.emailId" /></font>
					</div>

					<div class="row">
						<input type="submit" class="btn btn-info" value="Create User">
					</div>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>


