<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript">
	function showDtls(drp) {

		var d1 = drp.parentNode;
		var d2 = d1.parentNode;
		var d3 = d2.nextSibling.nextSibling;
		var d4 = d3.childNodes[1];
		var d5 = d4.nextSibling.nextSibling;

		if (d4.style.display == "none") {
			d4.setAttribute("style", "display:block;");
			d5.setAttribute("style", "display:inline-grid;");
		} else {
			d4.setAttribute("style", "display:none;");
			d5.setAttribute("style", "display:none;");

		}

		//document.createElement("div");

	}
	
	
		        
</script>
</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Recruit.ed</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="addcategory.htm">Add Job Category</a></li>
						<li><a href="addjob.htm">Post a Job</a></li>
						<li class="active"><a href="listjobs.htm">List Jobs<span
								class="badge">${sessionScope.noofjobs}</span></a></li>
						<li><a href="companyprofile.htm">Profile</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="./">Questions? Call us at
								1-866-204-6764 <span class="sr-only">(current)</span>
						</a></li>

						<li>Welcome, ${sessionScope.user.firstName}</li>

						<li><a href="logout.htm" class="btn btn-info" role="button">Logout</a></li>

					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>


		<div class="row">
			<table class="table table-striped">
				<thead>
					<tr>
						<th></th>
						<th>JOB TITLE</th>
						
						<th>JOB TYPE</th>
						<th>JOB EXPERIENCE</th>
						<th>LOCATION</th>
						<th>COMPANY</th>
						<th>POSTED ON</th>
						<th>STATUS</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="job" items="${jobs}" varStatus="loop">
						<tr>
							<td><button type="button" class="btn btn-default btn-xs"
									aria-label="Left Align" onclick="showDtls(this)">
									<span class="glyphicon glyphicon-triangle-bottom"
										aria-hidden="true"></span>
								</button></td>
							<td>${job.title}</td>
							
							<td>${job.jobType}</td>
							<td>${job.jobExperience}</td>
							<td>${job.jobLocation}</td>
							<td>${job.user.name}</td>
							<td>${job.jobpostedon}</td>
							<c:if test="${job.jobStatus == 'closed' }">

								<td style="color: red">Closed</td>
							</c:if>
							<td>Open</td>
							<td><a href="viewjob.htm?action=view"
								class="btn btn-info btn-xs" role="button">View</a></td>
						</tr>

						<tr>
							<td style="display: none;"></td>
							<td colspan="7" style="display: none;">
								<div class="row">
								<div class="col-md-12"><h2>Job Title: ${job.title}</h2></div>
									<div class="col-md-3">
										<h3>Job Description:</h3>
										${job.jobDescription}
									</div>

									<div class="col-md-3">
									<h3>Job Location:</h3>
										${job.jobLocation}
									</div>
									
									<div class="col-md-3">
									<h3>Job Posted on Date:</h3>
										${job.jobpostedon}
										
									</div>
									
									<div class="col-md-3">
									<h3>Other details:</h3>
									
										<ul>
										  <li>Job Experience : ${job.jobExperience}</li>
										  <li>Job Type : ${job.jobType}</li>
										  <li>Job Status : ${job.jobStatus}</li>
										 </ul>
									</div>
									
									

									<div class="col-md-4">
									<button type="button" class="btn btn-default btn-xs editbutton" value="Edit"
											aria-label="Left Align" data-toggle="modal" data-id="${loop.index}"
											data-target="#myModal">
											<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
										</button>
									
									</div>


								</div>
							</td>
						</tr>
					</c:forEach>


				</tbody>
			</table>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<form action="editjob.htm" method="post">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Change job details</h4>
						</div>
						<div class="modal-body">

							<div class="input-group">
								<label>Job Location: </label> <input type="text"
									class="form-control" value="${job.jobLocation}"
									name="joblocation" aria-label="Left Align">
							</div>
							
							<div class="input-group">
								<label>Job Description: </label> <input type="text"
									class="form-control" value="${job.jobDescription}"
									name="jobdescription" aria-label="Left Align">
							</div>
							
							
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default">Save
								changes</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>







	</div>
</body>
</html>